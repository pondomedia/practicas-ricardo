// una función rapidita de numeros pares

function pares(ultimonumero){
    let primerarreglo = []
    for(let i=0; i<ultimonumero; i++){
        if ( i%2===0 ) {
            primerarreglo.push(i)
        }       
    }
    return primerarreglo
}


// suma de digitos hasta reducirlo a un solo digito

function digital_root(numReducir){
    let numToArray = [...numReducir+''].map(n=>+n)
    numReducir = numToArray.reduce(function(a, b){ return a + b; })
    if (numReducir>10){
        digital_root(numReducir)
    }
    
    return numReducir
}

//contador de caracteres

function XO(xoxo){
    let countX = 0
    let countO = 0

    for ( let caracter of xoxo) {

        if (caracter === 'x' || caracter === 'X'){
            countX++
        } else
        if (caracter === 'o' || caracter === 'O'){
            countO++
        }
    }
    return countX === countO
}

//Iniciales

function abbrevName(nombres){
    let nombresArray = nombres.split(' ')
    let iniciales = []
    for( let i=0; i<nombresArray.length; i++){
        iniciales.push(nombresArray[i][0].toUpperCase())
    }
    return iniciales.join('.');
}


// puntaje de equipo

function points(games) {
    return games.reduce( (puntaje,b) => {
      let c = b.split(':')
      
      if(+c[0] > +c[1]){        
         return puntaje + 3
      }
      else if(+c[0] === +c[1]){        
         return puntaje + 1
      }
      else if(+c[0] < +c[1]){        
         return puntaje
      }
      
    },0)
    // your code here
  }

 

  /// contar numero

  function contar(numero, consecutives) {
	let cuentas = {}
	while(numero!==0){
			let digito = numero%10
			cuentas[digito] = (cuentas[digito]||0) + 1
			numero /= 10
	}
	return cuentas
}

function trouble(num1, num2) {
	let numeros1 = contar(num1)
	let numeros2 = contar(num2)
	
	return 
}


let morseCode = {
    "-----" : "0",
    ".----" : "1",
    "..---" : "2",
    "...--" : "3",
    "....-" : "4",
    "....." : "5",
    "-...." : "6",
    "--..." : "7",
    "---.." : "8",
    "----." : "9",
    ".-" : "a",
    "-..." : "b",
    "-.-." : "c",
    "-.." : "d",
    "." : "e",
    "..-." : "f",
    "--." : "g",
    "...." : "h",
    ".." : "i",
    ".---" : "j",
    "-.-" : "k",
    ".-.." : "l",
    "--" : "m",
    "-." : "n",
    "---" : "o",
    ".--." : "p",
    "--.-" : "q",
    ".-." : "r",
    "..." : "s",
    "-" : "t",
    "..-" : "u",
    "...-" : "v",
    ".--" : "w",
    "-..-" : "x",
    "-.--" : "y",
    "--.." : "z",
    ".-.-.-" : ".",
    "--..--" : ",",
    "..--.." : "?",
    "-.-.--" : "!",
    "-....-" : "-",
    "-..-." : "/",
    ".--.-." : "@",
    "-.--." : "(",
    "-.--.-" : ")",
    " " : " ",
    "---..." : ":",
    ".----." : "'",
  }


