const Phaser = require('phaser')
const Bomberman = require('./Bomberman')

class Principal extends Phaser.Scene {
    constructor(){
        super('Principal')
    }

    preload() {
        this.load.animation('bomberman-anim', 'assets/bomberman_anim.json')
        this.load.atlas('bomberman','/assets/bomberman.png', 'assets/bomberman_atlas.json')
    }

    create() {
        this.bomberman = new Bomberman(this,100,100)

    }

    update() {
    }

}

const game = new Phaser.Game({
    type: Phaser.CANVAS,
    autoFocus: true,
    backgroundColor: 0,
    width: 800,
    height: 800,
    parent: 'juego',
    physics: {
        default: 'arcade',
        arcade: {
            debug: true
        },
    },
    scene: [Principal],
})